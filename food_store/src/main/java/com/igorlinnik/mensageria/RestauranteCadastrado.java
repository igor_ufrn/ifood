package com.igorlinnik.mensageria;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;

import com.igorlinnik.dominio.Restaurante;
import io.smallrye.reactive.messaging.annotations.Blocking;
import org.eclipse.microprofile.reactive.messaging.Incoming;

import io.vertx.mutiny.pgclient.PgPool;

@ApplicationScoped
public class RestauranteCadastrado {

    @Inject
    PgPool pgPool;

    @Incoming("restaurantes")
    @Blocking
    public void receberRestaurante(io.vertx.core.json.JsonObject json) {
        Jsonb create = JsonbBuilder.create();
        Restaurante restaurante = create.fromJson(json.toString(), Restaurante.class);
        restaurante.persist(pgPool);
    }
}