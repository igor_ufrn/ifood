package com.igorlinnik.dto.mapper;

import com.igorlinnik.dominio.Prato;
import com.igorlinnik.dto.PratoDTO;

import org.mapstruct.Mapper;


@Mapper(componentModel = "cdi")
public interface PratoMapper {
    PratoDTO toDTO(Prato p);
}
