package com.igorlinnik.documentacao;

import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Contact;
import org.eclipse.microprofile.openapi.annotations.info.Info;
import org.eclipse.microprofile.openapi.annotations.info.License;

import javax.ws.rs.core.Application;


@OpenAPIDefinition(
        info = @Info(
                title="MICROSSERVIÇO FOOD STORE",
                description =   "Este <b>microsserviço</b> têm por finalidade permitir que as pessoas visualizem os pratos dos restaurantes, <br/>" +
                                "possam adicioná-los ao carrinho de compras para, em seguida, <b>realizar um pedido</b>.<br/>" +
                                "Este microsserviço demandará de maior <b>taxa de vazão</b>, pois atenderá um número de usuários <b>muito maior</b> que o<br/>" +
                                "microsserviço de RESTAURANTES, já que é aqui que todos os clientes entrarão para visualizar os produtos e, <br/>" +
                                "possivelmente, realizar um pedido.<br/>" +
                                "Sendo assim, todos os recursos fornecidos por este microsserviço são <b>reativos</b>. Isto faz com que a camada de roteamento<br/>" +
                                "do quarkus detecte que o recurso está retornando um recurso reativo e execute a requisição em uma <b>Thread de IO</b>(O chamado <b>vértice no contexto do Vext.x</b>),<br/> " +
                                "evitando a <b>troca de contexto</b> entre threads e, desta forma, diminuindo o <b>tempo de resposta da requisição</b>.<br/>" +
                                "Este microsserviço também faz uso de <b>mensageria</b>. Como falei anteriormente, este recebe, através do broker <b>ActiveMQ Artemis</b>, " +
                                "todos os restaurantes e pratos cadastrados no microsserviço <b>RESTAURANTE</b>.<br/> " +
                                "Além deste uso de mensageria, ele também faz utilização de um outro Broker de mensagens bem mais robusto: <b>Kafka</b>. Ao realizar um pedido,<br/> " +
                                "este é enviado ao microsserviço <b>ORDER</b> através do Broker de mensagens <b>Kafka</b>.<br/>" +
                                "Esta aplicação utiliza o banco de dados <b>POSTGRES</b>.",
                version = "1.0.0",
                contact = @Contact(
                        name = "Igor Linnik Câmara Araújo",
                        url = "https://www.linkedin.com/in/igor-linnik-8549462b/",
                        email = "igor.ufrn@gmail.com"),
                license = @License(
                        name = "Licença: Software Livre",
                        url = "https://pt.wikipedia.org/wiki/Software_gratuito#:~:text=Software%20gratuito%20ou%20freeware%20%C3%A9,e%20no%20segundo%20de%20livre."))
)
public class MicrosservicoFoodStoreApp extends Application{
}
