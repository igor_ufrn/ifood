package com.igorlinnik.resources;


import java.time.LocalDateTime;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.jboss.logging.Logger;

@Path("/helloWorldEnvioLogsRecource")
@Tag(name = "HelloWorldEnvioLogsRecource", description = "GELF envia LOGS para o LOGSTASH e este envia para o ELASTICSEARCH para que seja possível visualizar no KIBANA")
public class HelloWorldEnvioLogsRecource {

    private static final Logger LOG = Logger.getLogger(HelloWorldEnvioLogsRecource.class);

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String helloWorldEnvioLogsRecource() {
        LOG.info("ENVIANDO DADOS PARA O LOGSTASH, QUE IRA ENVIAR AO ELASTICSEARCH E IREMOS VISUALIZAR NO KIBANA");
        LOG.infov("DATA E HORA DA GERACAO (CLASSE) - {0} ", LocalDateTime.now());
        return "hello";
    }
}
