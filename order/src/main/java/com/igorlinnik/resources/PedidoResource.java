package com.igorlinnik.resources;

import java.util.List;

import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.igorlinnik.dominio.Localizacao;
import com.igorlinnik.dominio.Pedido;
import org.bson.types.ObjectId;

import io.quarkus.mongodb.panache.PanacheMongoEntityBase;
import io.vertx.core.Vertx;
import io.vertx.ext.bridge.PermittedOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.sockjs.SockJSBridgeOptions;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;
import io.vertx.mutiny.core.eventbus.EventBus;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

@Path("/pedidos")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = "PedidoResource")
public class PedidoResource {

    @Inject
    Vertx vertx;

    @Inject
    EventBus eventBus;

    /**
     * Evento para quando iniciar o router do Vert.x a gente registra o cara que vai receber
     * a requisição da WEB( requisição de 'localizações' ) para abrir o WEBSOCKET e vai ficar enviando
     * as notificações para ele
     */
    void startup(@Observes Router router) {
        router.route("/localizacoes*").handler(localizacaoHandler());
    }

    private SockJSHandler localizacaoHandler() {
        SockJSHandler handler = SockJSHandler.create(vertx);
        PermittedOptions permitted = new PermittedOptions();
        permitted.setAddress("novaLocalizacao");

        /**
         * Que tipo de permissão o cliente irá ter(Inbound - enviar eventos/Outbound - receber eventos)?
         * Qual o endereço que o cliente terá autorização?
         */
        SockJSBridgeOptions bridgeOptions = new SockJSBridgeOptions().addOutboundPermitted(permitted);
        handler.bridge(bridgeOptions);
        return handler;
    }

    @GET
    public List<PanacheMongoEntityBase> hello() {
        return Pedido.listAll();
    }

    /**
     *
     * Recebe localização atual de onde se encontra o pedido.
     *
     * @param idPedido
     * @param localizacao
     * @return
     */
    @POST
    @Path("{idPedido}/localizacao")
    public Pedido novaLocalizacao(@PathParam("idPedido") String idPedido, Localizacao localizacao) {
        Pedido pedido = Pedido.findById(new ObjectId(idPedido));

        pedido.setLocalizacaoEntregador(localizacao);
        String json = JsonbBuilder.create().toJson(localizacao);
        /**
         * Quando alguém enviar a localização para mim, irei enviar também para o EventBus!
         */
        eventBus.sendAndForget("novaLocalizacao", json);
        pedido.persistOrUpdate();
        return pedido;
    }
}
