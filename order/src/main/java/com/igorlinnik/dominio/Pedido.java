package com.igorlinnik.dominio;

import java.util.List;

import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@AllArgsConstructor
@NoArgsConstructor
@MongoEntity(collection = "pedidos", database = "order")
//@MongoEntity(collection = "pedidos")
@ToString
public class Pedido extends PanacheMongoEntity {

    private String cliente;

    private List<Prato> pratos;

    private Restaurante restaurante;

    private String entregador;

    private Localizacao localizacaoEntregador;

}
