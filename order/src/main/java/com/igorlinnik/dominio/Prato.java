package com.igorlinnik.dominio;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.Decimal128;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Prato {

    private String nome;

    private String descricao;

    //private Decimal128 preco;

    private BigDecimal preco;
}
