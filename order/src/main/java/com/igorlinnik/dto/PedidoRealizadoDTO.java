package com.igorlinnik.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PedidoRealizadoDTO {

    private List<PratoPedidoDTO> pratos;

    private RestauranteDTO restaurante;

    private String cliente;

}
