package com.igorlinnik.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PratoPedidoDTO {

    private String nome;

    private String descricao;

    private BigDecimal preco;



}
