package com.igorlinnik.mensageria.controller;

import com.igorlinnik.dominio.Pedido;
import com.igorlinnik.dto.PedidoRealizadoDTO;
import com.igorlinnik.mensageria.service.ESService;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.modelmapper.ModelMapper;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.bind.JsonbBuilder;

@ApplicationScoped
public class PedidoRealizadoIncoming {

    @Inject
    ESService elastic;

    @Incoming("pedidos")
    public void lerPedidos(PedidoRealizadoDTO dto) {
        ModelMapper modelMapper = new ModelMapper();
        Pedido p = modelMapper.map(dto, Pedido.class);
        String json = JsonbBuilder.create().toJson(dto);
        elastic.index("pedidos", json);
        p.persist();

    }
}
