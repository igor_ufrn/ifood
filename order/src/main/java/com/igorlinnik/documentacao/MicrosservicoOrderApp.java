package com.igorlinnik.documentacao;

import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Contact;
import org.eclipse.microprofile.openapi.annotations.info.Info;
import org.eclipse.microprofile.openapi.annotations.info.License;

import javax.ws.rs.core.Application;


@OpenAPIDefinition(
        info = @Info(
                title="MICROSSERVIÇO ORDER",
                description =   "Este <b>microsserviço</b> têm por finalidade principal realizar o processamento dos pedidos feitos no microsserviço<br/> " +
                                "<b>FOOD STORE</b> e entregues através do broker de mensageria <b>Kafka</b>.<br/>" +
                                "Este microsserviço utiliza uma extensão denominada <b>GELF</b>. Esta extensão envia <b>LOGS</b> para o <b>LOGSTASH</b> e<br/>" +
                                "este envia os <b>LOGS</b> para o <b>NoSQL ELASTICSEARCH</b> (Log centralizado). Com os LOGS no ELASTICSEARCH podemos visualizá-los no <b>KIBANA</b>.<br/>" +
                                "Este microsserviço utiliza como banco de dados um banco <b>NoSQL</b> e <b>orientado a documentos</b> denominado de <b>MongoDB</b>. Nele são armazenados todos os pedidos recebidos do Broker <b>Kafka</b>.<br/>" +
                                "Estes mesmos pedidos também são enviados ao <b>ELASTICSEARCH</b>, mas desta vez, não através do <b>LOGSTASH</b>, mas sim, através de um serviço <b>CLIENT REST</b> de alto nível.<br/>" +
                                "Quando a aplicação está sendo iniciada ela dispara vários <b>eventos informativos</b> e podemos observar estes eventos para realizar algumas tarefas importantes. Dois destes eventos são<br/>" +
                                "muito importantes, os eventos <b>StartupEvent</b> e <b>ShutdownEvent</b>:<br/><br/>" +
                                "<b>StartupEvent</b>: Este evento é disparado quando a aplicação está <b>sendo iniciada</b>. Após sermos <b>notificado</b> deste evento, instanciamos um <b>CLIENT REST</b> de alto nível do <b>ELASTICSEARCH</b><br/>" +
                                "e o disponibilizamos como um <b>BEAN</b> para que toda a aplicação possa usá-lo e, assim, poder <b>enviar</b> dados ao <b>ELASTICSEARCH</b>. Observe que foi aberta uma <b>conexão com o ELASTICSEARCH.</b><br/><br/>" +
                                "<b>ShutdownEvent</b>: Este evento é disparado quando a aplicação está <b>sendo desligada</b>. Após sermos notificado deste evento, <b>fechamos a conexão</b> que foi aberta no evento anterior.<br/><br/>" +
                                "Este <b>microsserviço</b> também utiliza <b>reatividade</b> para simular o <b>posicionamento</b> exato de um entregador de um pedido, enviando para uma <b>aplicação cliente</b> a latitude e a longitude do entregador com o pedido.<br/>" +
                                "Desta vez, utilizamos diretamente as bibliotecas core: <b>Vertx</b> e <b>EventBus</b>. Quando as rotas do <b>Vert.x</b> estiverem sendo <b>geradas</b> (sabemos disto observando um evento), <b>registramos</b> uma rota que vai receber<br/>" +
                                "as <b>requisições</b> da WEB (path <b>'/localizações'</b>) de clientes que desejam abrir um <b>WEBSOCKET</b> para ficar recebendo os eventos de nova localização do entregador do pedido.<br/>" +
                                "O <b>WebSocket</b> é um protocolo <b>assíncrono, bidirecional e full-duplex</b> que fornece um canal de comunicação " +
                                "em uma <b>única conexão TCP</b>. Com a API WebSocket, ele fornece <b>comunicação bidirecional</b> entre o site e um servidor remoto. " +
                                "Os <b>WebSockets</b> resolvem muitos problemas que impediam o protocolo HTTP de ser adequado para uso em <b>aplicativos modernos de tempo real</b>. " +
                                "Os WebSockets não precisam abrir várias conexões HTTP, eles fornecem uma <b>redução do tráfego</b> de rede desnecessário e <b>reduzem a latência</b>.<br/>" +
                                "Este microsserviço implementa o <b>microprofile HEALTH</b> que nos permite saber sobre a saúde da nossa aplicação através de alguns endpoints principais:<br/><br/>" +
                                "<b>/health:</b> Faz uma série de <b>verificações nos componentes</b> utilizados pela aplicação para saber se a aplicação está <b>saudável</b><br/><br/>" +
                                "<b>/health/live:</b> Apenas verifica se aplicação responde, sem fazer <b>nenhuma verificação</b> nos componentes. Em um <b>orquestrador de container</b>, podemos usar este endpoint para indicar se o <b>container</b> deve ser <b>reiniciado</b>.<br/><br/>" +
                                "<b>/health/ready:</b> Indica se a aplicação está pronta para <b>receber requisições</b>. Em um <b>orquestrador de container</b>, podemos usar este endpoint para indicar ao orquestrador que nossa aplicação já pode <b>receber conexões</b>.<br/><br/>",

                version = "1.0.0",
                contact = @Contact(
                        name = "Igor Linnik Câmara Araújo",
                        url = "https://www.linkedin.com/in/igor-linnik-8549462b/",
                        email = "igor.ufrn@gmail.com"),
                license = @License(
                        name = "Licença: Software Livre",
                        url = "https://pt.wikipedia.org/wiki/Software_gratuito#:~:text=Software%20gratuito%20ou%20freeware%20%C3%A9,e%20no%20segundo%20de%20livre."))
)
public class MicrosservicoOrderApp extends Application {
}
