package com.igorlinnik.dto;

import javax.validation.ConstraintValidatorContext;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.igorlinnik.dominio.Restaurante;
import com.igorlinnik.dto.config.DTO;
import com.igorlinnik.dto.config.ValidDTO;

@ValidDTO
public class AdicionarRestauranteDTO implements DTO {

    @Pattern(regexp = "[0-9]{2}\\.[0-9]{3}\\.[0-9]{3}\\/[0-9]{4}\\-[0-9]{2}")
    @NotNull
    public String cnpj;

    @Size(min = 3, max = 30)
    public String nomeFantasia;

    public LocalizacaoDTO localizacao;

    @Override
    public boolean isValid(ConstraintValidatorContext constraintValidatorContext) {
        constraintValidatorContext.disableDefaultConstraintViolation();
        if (Restaurante.find("cnpj", cnpj).count() > 0) {
            constraintValidatorContext.buildConstraintViolationWithTemplate("CNPJ já cadastrado")
                    /* Qual a propriedade que esta em desacordo com as regras */
                    .addPropertyNode("cnpj")
                    /* Adiciona ao contexto a violação da regra. */
                    .addConstraintViolation();
            return false;
        }
        return true;
    }

}
