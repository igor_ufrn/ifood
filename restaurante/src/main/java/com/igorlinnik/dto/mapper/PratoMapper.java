package com.igorlinnik.dto.mapper;

import com.igorlinnik.dominio.Prato;
import com.igorlinnik.dto.AdicionarPratoDTO;
import com.igorlinnik.dto.AtualizarPratoDTO;
import com.igorlinnik.dto.PratoDTO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;



@Mapper(componentModel = "cdi")
public interface PratoMapper {

    PratoDTO toDTO(Prato p);

    Prato toPrato(AdicionarPratoDTO dto);

    void toPrato(AtualizarPratoDTO dto, @MappingTarget Prato prato);

}
