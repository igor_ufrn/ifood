package com.igorlinnik.dto.config;

import javax.validation.ConstraintValidatorContext;

/**
 * A idéia é que todo DTO saiba se validar.
 */
public interface DTO {

    default boolean isValid(ConstraintValidatorContext constraintValidatorContext) {
        return true;
    }
}
