package com.igorlinnik.dto.config;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;
/* Segue o padrão das outras anotações do hibernate validator */
@Target({ ElementType.TYPE, ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = { ValidDTOValidator.class })
@Documented
public @interface ValidDTO {
    /* Mensagem padrão de validação do DTO  */
    String message() default "{com.igorlinnik.dto.config.ValidDTO.message}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
