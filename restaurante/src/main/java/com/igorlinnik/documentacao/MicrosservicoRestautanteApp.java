package com.igorlinnik.documentacao;

import org.eclipse.microprofile.openapi.annotations.Components;
import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeType;
import org.eclipse.microprofile.openapi.annotations.info.Contact;
import org.eclipse.microprofile.openapi.annotations.info.Info;
import org.eclipse.microprofile.openapi.annotations.info.License;
import org.eclipse.microprofile.openapi.annotations.security.OAuthFlow;
import org.eclipse.microprofile.openapi.annotations.security.OAuthFlows;
import org.eclipse.microprofile.openapi.annotations.security.SecurityScheme;

import javax.ws.rs.core.Application;


@OpenAPIDefinition(

        components = @Components(
                securitySchemes = {@SecurityScheme(securitySchemeName = "ifood-oauth",
                                    type = SecuritySchemeType.OAUTH2,
                                    flows = @OAuthFlows(
                                        password = @OAuthFlow(tokenUrl = "http://172.17.0.1:8180/auth/realms/ifood/protocol/openid-connect/token", scopes = {})
                                    )
                )}
        ),
        /*
        tags = {
                @Tag(name="Operações da API", description="Conjunto de todas as operações permitidas")
        },
        */
        info = @Info(
                title="MICROSSERVIÇO RESTAURANTE",
                description =   "Este <b>microsserviço</b> têm por finalidade fazer o <b>cadastro de restaurante e pratos</b>.<br/>" +
                                "Quem irá operar este sistema serão os <b>donos dos restaurantes</b>.<br/>" +
                                "Os pratos ofertados por cada restaurante ficarão disponíveis para<br/>" +
                                "que os clientes possam adicioná-los a um pedido através de um um <b>outro</b> microsserviço, denominado de <b>FOOD STORE</b>.<br/> " +
                                "Este microsserviço se <b>comunica</b> com o microsserviço <b>FOOD STORE</b> de forma <b>assíncrona</b>,<br/> " +
                                "enviando todos os restaurantes e pratos cadastrados.<br/> " +
                                "Para isto, é utilizado o <b>Broker</b> de mensagens <b>ActiveMQ Artemis.</b><br/>" +
                                "Somente proprietários de restaurantes, ou seja, pessoas com a permissão <b>PROPRIETARIO</b></br>" +
                                "podem acessar os recursos, logo eles devem ser primeiramente cadastrados no servidor de<br/> " +
                                "autorização <b>Keycloak</b>. Após isto, devem se autenticar no sistema com usuário e senha para<br/> " +
                                "receber um <b>token JWT</b> que os permitirá executar as operações.<br/>" +
                                "Algumas das tecnologias utilizadas nos <b>testes</b> deste microsserviço são: <b>JUnit5, Rest Assured, Approval Tests, <br/>" +
                                "Test Containers e Database Rider</b>.</br>" +
                                "Para <b>mapeamento</b> de dados entre as classes de <b>domínio</b> e as classes de <b>DTO</b><br/>" +
                                "foram utilizados tanto o <b>MapStruct</b> quando o <b>ModelMapper</b>.<br/>" +
                                "Foi utilizado também o <b>Lombok</b> para gerar alguns métodos de forma mais produtiva.<br/>" +
                                "Para <b>validação</b> de alguns dados das classes de DTOs e domínio, foi utilizado o <b>Hibernate Validator</b>.<br/>" +
                                "Para fazer o <b>tracing</b> das operações do microsserviço, é utilizado o <b>Jaeger</b><br/>" +
                                "através das extensões <b>opentracing</b> e <b>opentracing-jdbc</b>, permitindo<br/>" +
                                "até o rastreamento a nível de banco de dados. No tracing irá constar<br/>" +
                                "todas as operações realizadas no banco de dados (Atualmente <b>POSTGRES</b>).<br/>" +
                                "Para fazer o <b>monitoramento</b> do microsserviço foi utilizado o <b>Prometheus</b> e o <b>Grafana</b>.<br/>" +
                                "Esta aplicação utiliza o microprofile <b>metrics</b> através da extensão metrics do Quarkus. Com isto, podemos contabilizar <br/>" +
                                "várias informações da nossa aplicação, como por exemplo:<br/><br/>" +
                                "<b>@SimplyTimed(name =\"Tempo para processamento de um recurso\")</b><br/>" +
                                "<b>@Counted(name =\"Quantidade de vezes em que um recurso foi executado\")</b><br/><br/>" +
                                "Para a manutenção do versionamento e fazer migração do banco de dados foi utilizado o <b>Flyway</b>.<br/>",
                version = "1.0.0",
                contact = @Contact(
                        name = "Igor Linnik Câmara Araújo",
                        url = "https://www.linkedin.com/in/igor-linnik-8549462b/",
                        email = "igor.ufrn@gmail.com"),
                license = @License(
                        name = "Licença: Software Livre",
                        url = "https://pt.wikipedia.org/wiki/Software_gratuito#:~:text=Software%20gratuito%20ou%20freeware%20%C3%A9,e%20no%20segundo%20de%20livre."))
)
public class MicrosservicoRestautanteApp extends Application {
}
