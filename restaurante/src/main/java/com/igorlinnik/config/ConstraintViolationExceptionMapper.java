package com.igorlinnik.config;

import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Provider que faz o mapeamento de alguma exceção específica.
 * Neste caso, estamos tratanto a exceção ConstraintViolationException.
 * Mas podemos criar qualquer exceção e fazer o mapeamento.
 */
@Provider
public class ConstraintViolationExceptionMapper implements ExceptionMapper<ConstraintViolationException> {

    @Override
    public Response toResponse(ConstraintViolationException exception) {
        return Response.status(Status.BAD_REQUEST).entity(ConstraintViolationResponse.of(exception)).build();
    }

}
