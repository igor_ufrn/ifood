package com.igorlinnik.resources;

import com.igorlinnik.dominio.Restaurante;
import com.igorlinnik.dto.AtualizarRestauranteDTO;
import com.igorlinnik.lifecycle.RestauranteTestLifecycleManager;
import com.igorlinnik.utils.TokenUtils;
import org.approvaltests.Approvals;
import org.junit.Assert;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.Response.Status;

import com.github.database.rider.cdi.api.DBRider;
import com.github.database.rider.core.api.configuration.DBUnit;
import com.github.database.rider.core.api.configuration.Orthography;
import com.github.database.rider.core.api.dataset.DataSet;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.specification.RequestSpecification;

/* Permite popular o banco de dados */
@DBRider
/* DBRider é construído em cima do DBUnit. Aqui setamos a estratégia para nomes de tabelas e colunas */
@DBUnit(caseInsensitiveStrategy = Orthography.LOWERCASE)
/*
Indica que se trata de uma classe de testes do quarkus. Assim o mesmo fornece alguns recursos
do quarkus que são essenciais à execução de testes.
*/
@QuarkusTest
/* Adiciona este ciclo de vida ao teste.  */
@QuarkusTestResource(RestauranteTestLifecycleManager.class)
public class RestauranteResourceTest {

    private String token;

    @BeforeEach
    public void gereToken() throws Exception {

        /* O segundo parâmetro permite sobrescrever alguma propriedade do JSON. Basta passar um mapa de parâmetros */
        token = TokenUtils.generateTokenString("/JWTProprietarioClaims.json", null);
    }



    @Test
    /* src/test/resources/restaurantes-cenario-1.yml */
    @DataSet("restaurantes-cenario-1.yml")
    public void testBuscarRestaurantes() {
        String resultado = given()
                .when().get("/restaurantes")
                .then()
                .statusCode(Status.OK.getStatusCode())
                .extract().asString();

        /**
         * Verifica se existe um arquivo Classe.metodo.approved.json (RestauranteResourceTest.testBuscarRestaurantes.approved.json)        *
         * Este arquivo indica o que esperamos que seja recebido como
         * resposta e deve estar localizado no mesmo diretório da classe de testes.
         */
        Approvals.verifyJson(resultado);
    }

    private RequestSpecification given() {
        return RestAssured.given()
                .contentType(ContentType.JSON).header(new Header("Authorization", "Bearer " + token));
    }

    //Exemplo de um teste de PUT
    @Test
    @DataSet("restaurantes-cenario-1.yml")
    public void testAlterarRestaurante() {
        AtualizarRestauranteDTO dto = new AtualizarRestauranteDTO();
        dto.nomeFantasia = "novoNome";
        Long parameterValue = 123L;
        given()
                .with().pathParam("id", parameterValue)
                .body(dto)
                .when().put("/restaurantes/{id}")
                .then()
                .statusCode(Status.NO_CONTENT.getStatusCode())
                .extract().asString();

        Restaurante findById = Restaurante.findById(parameterValue);

        //poderia testar todos os outros atribudos
        Assert.assertEquals(dto.nomeFantasia, findById.nome);

    }

}
