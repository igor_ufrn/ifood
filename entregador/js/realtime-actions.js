function init() {
    registerHandler();
};

function registerHandler() {
    //Cria um EventBus
    var eventBus = new EventBus('http://localhost:8082/localizacoes');
    /**
     * Quanto este EventBus estiver aberto, registre um novo handler.
     * Quando chegar um evento de 'novaLocalizacao', recupere o 
     * elemento cujo id é 'localizacoes' e faça a concatenação
     * com o body do evento (corpo da mensagem).
     */
    eventBus.onopen = function () {
        eventBus.registerHandler('novaLocalizacao', function (error, message) {
            document.getElementById('localizacoes').value +=message.body+'\n\n----------------\n\n';
        });
    }
};